package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Cameron on 10/19/16.
 */
public class AdView extends View {
    public AdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
