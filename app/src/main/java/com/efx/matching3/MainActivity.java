package com.efx.matching3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int activePlayer = 0;
    // 0 = Keaton, 1 = Kaden.

    boolean gameIsActive = true;

    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    // 2 means unplayed/empty space

    int[][] winningPositions ={{0,1,2}, {3,4,5}, {6,7,8}, {0,4,8}, {0,3,6}, {1,4,7}, {2,5,8}, {2,4,6}};

    public void dropIn(View view) {

        ImageView counter = (ImageView) view;
        
        int tagCounter = Integer.parseInt(counter.getTag().toString());

        if (gameState[tagCounter] == 2 && gameIsActive) {

        gameState[tagCounter] = activePlayer;

            counter.setTranslationY(-1000f);

            if (activePlayer == 0) {

                counter.setImageResource(R.drawable.circle);

                activePlayer = 1;

            } else {

                counter.setImageResource(R.drawable.x);

                activePlayer = 0;

            }

            counter.animate().translationYBy(1000f).rotation(360).setDuration(300);

            for (int[] winningPosition : winningPositions){

                if (gameState[winningPosition[0]] == gameState[winningPosition[1]] &&
                        gameState[winningPosition[1]] == gameState[winningPosition[2]] &&
                        gameState[winningPosition[0]] != 2) {

                    //Someone won!

                    gameIsActive = false;

                    String winner = "X";

                    if (gameState[winningPosition[0]] == 0){

                        winner = "Circle";

                    }


                    TextView winnerMessage = (TextView) findViewById(R.id.winnerMessage);

                    winnerMessage.setText(winner + " has won!");

                    LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
                    layout.animate().alpha(0.9f).setDuration(2000);
                    layout.setVisibility(View.VISIBLE);

                } else {

                    boolean gameisOver = true;

                    for (int counterState : gameState){
                        if (counterState == 2) gameisOver = false;
                    }

                    if (gameisOver){

                        TextView winnerMessage = (TextView) findViewById(R.id.winnerMessage);

                        winnerMessage.setText("It's a draw!");

                        LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
                        layout.animate().alpha(0.9f).setDuration(2000);
                        layout.setVisibility(View.VISIBLE);

                    }

                }


            }
        }
    }

    public void restart (View view){

        gameIsActive = true;

        LinearLayout layout = (LinearLayout)findViewById(R.id.playAgainLayout);
        layout.animate().alpha(0f).setDuration(2000);
        layout.setVisibility(View.INVISIBLE);

        activePlayer = 0;

        for (int i = 0; i < gameState.length; i++){

            gameState[i] = 2;
        }

        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        for (int i = 0; i < gridLayout.getChildCount(); i++){

            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        getWindow().setStatusBarColor(Color.TRANSPARENT);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
